import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from "../home/home";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

	private username: string;
	private password: string;
	
	constructor(public navCtrl: NavController) {
		this.username = '';
		this.password = '';
	}
	
	public login() {
		if (this.username === 'a' && this.password === 'b') {
			this.navCtrl.push(HomePage);
		}
	}

}
